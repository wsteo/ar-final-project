using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VBTN2 : MonoBehaviour
{
    public VirtualButtonBehaviour Vb;
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        Vb.RegisterOnButtonPressed(OnButtonPressed);
        Vb.RegisterOnButtonReleased(OnButtonReleased);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
       Debug.Log("Animation Played");
       anim.SetBool("move", true);
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Debug.Log("Animation Stopped");
        anim.SetBool("move", false);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Video : MonoBehaviour
{
    public GameObject MainObject;
    public GameObject plane;

	public void OnPressVideo()
    {
        plane.SetActive(true);
    }

    public void OnReleaseVideo()
    {
        plane.SetActive(false);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{

    [SerializeField]
    private VideoPlayer[] videoPlane;
    private int _foodID;
    /*
    public int FoodID{
        get{return _foodID;}
        set{FoodID = _foodID;}
    }
    */

    public void setVideoClip(FoodInformation foodInformation){
        _foodID = foodInformation.FoodID;
        videoPlane[_foodID].clip = foodInformation.FoodVideo;
        Debug.Log("Add " + foodInformation.FoodVideo);
    }
}

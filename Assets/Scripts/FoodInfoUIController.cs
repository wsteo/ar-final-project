using UnityEngine;
using TMPro;

public class FoodInfoUIController : MonoBehaviour {
    [SerializeField]
    private GameObject foodInformationUI;

    [SerializeField]
    private TMP_Text foodName;

    [SerializeField]
    private TMP_Text foodDescription;

    [SerializeField]
    private TMP_Text foodPrice;

    private FoodInformation foodInformation;

    private void Start() {
        foodInformationUI.SetActive(false);
    }

    public void EnableFoodInformation(){
        foodInformationUI.SetActive(true);
    }

    public void DisableFoodInformation(){
        foodInformationUI.SetActive(false);
    }

    public void SetFoodData(FoodInformation _foodInformationData){
        foodInformation = _foodInformationData;
        foodName.text = foodInformation.FoodName;
        foodDescription.text = foodInformation.FoodDescription;
        foodPrice.text = foodInformation.FoodPrice;
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VBTN : MonoBehaviour
{
    public GameObject plane;
    public VirtualButtonBehaviour Vb;
    // Start is called before the first frame update
    void Start()
    {
        Vb.RegisterOnButtonPressed(OnButtonPressed);
        Vb.RegisterOnButtonReleased(OnButtonReleased);
        plane.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        plane.SetActive(true);
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        plane.SetActive(false);
    }
}

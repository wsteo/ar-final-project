using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(fileName = "FoodInformation", menuName = "ar-final-project/FoodInformation", order = 0)]
public class FoodInformation : ScriptableObject {
    
    [SerializeField]
    private int _foodID;

    [SerializeField]
    private string _foodName;

    [SerializeField]
    private VideoClip _foodVideo;

    [SerializeField][TextArea(10,20)]
    private string _foodDescription;

    [SerializeField]
    private string _foodPrice;
    
    public string FoodName => _foodName;

    public string FoodDescription => _foodDescription;

    public string FoodPrice => _foodPrice;

    public VideoClip FoodVideo => _foodVideo;

    public int FoodID => _foodID;
}